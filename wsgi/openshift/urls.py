from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'openshift.home.views.home', name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tour/', include('tour.urls')),
    url(r'^schooltour/', include('schooltour.urls')),
)
