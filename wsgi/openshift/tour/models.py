#!coding:utf-8
from django.db import models


# Create your models here.
class Lesson (models.Model):
    slug = models.SlugField(primary_key=True, max_length=250, unique=True)
    title =models.CharField('Название урока',max_length=50)
    text=models.TextField('Текст урока')
    code=models.TextField('Код')
    datetime=models.DateTimeField('Дата и время')

    class Meta:
        verbose_name = 'Урок'
        verbose_name_plural = 'Уроки'
	ordering=['datetime']
 


