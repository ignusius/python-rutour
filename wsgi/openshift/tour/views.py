from tour.models import Lesson
from django.views.generic import DetailView,ListView
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404

# Create your views here.

class IndexView(ListView):
    model = Lesson
    template_name = 'tour/index.html'
    context_object_name = 'lessons'

class LessonView(DetailView):
    model = Lesson
    template_name = 'tour/lesson.html'
    context_object_name = 'lessons'

    def get_absolute_url(self):
        return reverse('lessons', args=[self.slug])

    def get_object(self):
        return get_object_or_404(Lesson, slug__iexact=self.kwargs['pk'])
  





