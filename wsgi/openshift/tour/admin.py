#!coding:utf-8
from django.contrib import admin
from tour.models import Lesson

# Register your models here.


class LessonAdmin(admin.ModelAdmin):
    list_display = ['slug','title','datetime']
    list_filter = ['datetime']

admin.site.register(Lesson,LessonAdmin)

