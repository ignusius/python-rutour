from django.conf.urls import patterns, include, url
from tour.views import IndexView,LessonView
from openshift import settings

from django.contrib.staticfiles.urls import staticfiles_urlpatterns




urlpatterns = patterns('',
     url(r'^$', IndexView.as_view()),
     url(r'^(?P<pk>[-\w]+)/$', LessonView.as_view()),
     #url(r'^(?P.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
     #url(r'^404/$', Error404View.as_view()),
     #url(r'^(?P<id>\d+)/$', views.Error.as_view(), name='error'),
)


urlpatterns += staticfiles_urlpatterns()
