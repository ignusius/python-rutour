# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Lesson'
        db.create_table(u'tour_lesson', (
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=250, primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('code', self.gf('django.db.models.fields.TextField')()),
            ('date', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 1, 24, 0, 0))),
            ('time', self.gf('django.db.models.fields.TimeField')(default='10:46:58')),
        ))
        db.send_create_signal(u'tour', ['Lesson'])


    def backwards(self, orm):
        # Deleting model 'Lesson'
        db.delete_table(u'tour_lesson')


    models = {
        u'tour.lesson': {
            'Meta': {'object_name': 'Lesson'},
            'code': ('django.db.models.fields.TextField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 1, 24, 0, 0)'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '250', 'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'time': ('django.db.models.fields.TimeField', [], {'default': "'10:46:58'"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['tour']