import sys
import re 
editor=JSObject(ace).edit("sc_editor")
editor.getSession().setMode("ace/mode/python")
context={}
def escape_chevrons(data) :
    data=re.sub(">","&gt;",data) 
    data=re.sub("<","&lt;",data) 
    return data

def write(data,escape=True):
    data=str(data)
    if escape : data=escape_chevrons(data)
    #doc["sc_console"].value += str(data)
    content=doc["sc_console"].innerHTML + str(data)
    #alert(doc["sc_console"].innerHTML)
    doc["sc_console"].innerHTML = content 
    doc["sc_console"].scrollTop = doc["sc_console"].scrollHeight 


sys.stdout.write = write
sys.stderr.write = write

#if sys.has_local_storage:
#    from local_storage import storage
#else:
#    storage = False

def echo():
    alert(doc["zone"].value)

def sc_clear() : doc["sc_console"].innerHTML=""
def sc_run():
    import time
    import traceback 
    #doc["sc_console"].innerHTML=''
    #write("<b>Выполнение</b>\n",escape=False)
    src = win.editor.getValue()
    #if storage:
    #   storage["py_src"]=src
    #
    t0 = time.time()
    try:
        exec(src,context,context)
    except Exception as exc:
        #write("<font color='#f00'>",escape=False)
        traceback.print_exc()
        #write("</font>",escape=False)

def sc_commande():
    import traceback

    src = doc["sc_command"].value
    lacom="<span class='command'><b>&gt;&gt;&gt;</b>&nbsp;"+escape_chevrons(src)+"</span>\n"
    write(lacom,escape=False)
    #if storage:
    #   storage["py_src"]=src
    #
    try:
        #alert(context)
        #c=compile(src,"Command Line","eval")
        #res=eval(src,context)
        res=exec(src,context,context)
        #alert(context)
        if res is not None : write(str(res)+"\n",escape=True)
    except Exception as exc:
        #write("<font color='#f00'>",escape=False)
        traceback.print_exc()
        #write("</font>",escape=False)
        pass
    else :
        doc["sc_command"].value=""

